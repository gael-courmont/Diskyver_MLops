FROM python:latest


WORKDIR /app


RUN  pip3 install gunicorn uvloop httptools
RUN pip3 install sklearn
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY services/ /app/services
COPY model/ /app/model

ENTRYPOINT /usr/local/bin/gunicorn \
-b 0.0.0.0:5000 \
-w 4 \
-kuvicorn.workers.UvicornWorker main:app --chdir /app/services
