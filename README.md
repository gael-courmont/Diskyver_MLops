# Diskyver deploiement de ml


# Déploiement d'une api donnant le résultat d'une analyse de call id

Ce repository contient une api python permettant de déployer un modèle de machine learning analysant les call id

## Installation

### librairies

-Après avoir cloné le repository ouvrir un terminal

-aller dans votre dossier

-Installer les librairies par pip : taper dans le terminal: pip install -r requirements.txt

-Vous pouvez maintenant lancer l'api.

## lancer api
Aller dans votre terminal, dans le dossier de votre repo
Aller dans services ("cd services")
Taper : python main.py (lancer main.py)

L'api se lance et affiche l'url 

## utiliser api
demander une prediction url/prediction/{votre call id}



# lancer une docker image dédié a faire tourner notre api

Installer docker:https://docs.docker.com/engine/install/ubuntu/

##créer docker image

ouvrir terminal et aller dans le dossier du repo

run la commande: docker build --tag nom-de-l'image

## lancer la docker images dans un container
commande: docker run -p (numero port ecoute):(port de docker image) (nom image)

exemple run: docker run -p 8000:5000 python-api

Vous avez maintenant accès à l'api à l'adresse 0.0.0.0:8000/

//

