import preprocessing
import pickle
import numpy as np
import pandas as pd
from tensorflow import keras
import sklearn

def dbscan_predict(model, X):

    nr_samples = X.shape[0]

    y_new = np.ones(shape=nr_samples, dtype=int) * -1
    for i in range(nr_samples):
        diff = model.components_ - X[i, :]  # NumPy broadcasting
        dist = np.linalg.norm(diff, axis=1)  # Euclidean distance
        shortest_dist_idx = np.argmin(dist)
        if dist[shortest_dist_idx] < model.eps:
            y_new[i] = model.labels_[model.core_sample_indices_[shortest_dist_idx]]

    return y_new


def load_model_dbscan():
    """
        return dbscan and encoder mode
    """
    dbscan=pickle.load( open( "../model/DBSCAN.pkl", "rb" ) )

    return dbscan

def load_encoder():
    encoder = keras.models.load_model('../model/encoder_model.h5')
    return encoder


def preprocess(call_id):
    #preprocessing
    df = pd.DataFrame(columns=["Call_id"])
    df = df.fillna(0)
    df.loc[0,"Call_id"]=call_id

    data=preprocessing.analyse_callID(df)
    del data["Call_id"]
    return data


def get_prediction(call_id:str):
    encoder = load_encoder()
    dbscan=load_model_dbscan()
    data=preprocess(call_id)
    encoded_data=encoder.predict(data)
    prediction=dbscan_predict(dbscan,encoded_data)
    return(prediction[0])