import fastapi
import uvicorn
import model_sim
from fastapi import Path,HTTPException,status



app = fastapi.FastAPI(tittle="clustering_call_id",description="aapi to predict if a call id is suspicious or not")

@app.get("/")
async def root():
    return {"message": "prediction api to predict call id pattern",
            "usage":"call_id prediction /prediction/{call_id} "}

@app.get("/prediction/{call_id}")
def cid_pred(call_id:str=Path(None,description="the call id you'd like to be analyse")):
    try:
        pred=model_sim.get_prediction(call_id)
    except:
        raise HTTPException(status_code=520,detail="prediction fonction stoppped")
    print(pred)
    if (pred==-1):
        return {"prediction": str(pred),"warning":1}

    return {"prediction" : str(pred) ,"warning":0}



if __name__=='__main__':
    uvicorn.run(app)